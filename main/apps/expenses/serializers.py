from rest_framework import serializers

from main.apps.expenses.models import Bank, Transaction


class BankSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Bank
        fields = ("id", "currency", "amount", "name")


class TransactionSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Transaction
        fields = ("id", "bank", "amount", "currency", "message", "transaction_type")
