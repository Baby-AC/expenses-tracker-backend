from rest_framework import viewsets, response, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action

from main.apps.expenses.models import Bank, Transaction
from main.apps.expenses.serializers import BankSerializer, TransactionSerializer
from main.apps.expenses.utils import convert_to_currency
from main.apps.expenses.constants import DEBIT


class BankViewset(viewsets.ModelViewSet):
    serializer_class = BankSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        user = self.request.user
        queryset = Bank.objects.filter(user=user)
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class TransactionViewset(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        queryset = Transaction.objects.all()
        bank = self.request.query_params.get("bank")
        if bank is not None:
            queryset = queryset.filter(bank=bank)
        return queryset

    @action(detail=False, methods=['post'])
    def add_transaction(self, request, pk=None):
        serializer = TransactionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        amount = serializer.validated_data["amount"]
        transaction_type = serializer.validated_data.get("transaction_type", DEBIT)
        transaction_currency = serializer.validated_data["currency"]
        bank = serializer.validated_data["bank"]
        serializer.save()
        new_amount = convert_to_currency(amount, transaction_currency, bank.currency)
        if bank.currency != transaction_currency and new_amount is not None:
            amount = new_amount
        amount = amount * transaction_type
        bank.amount = bank.amount + amount
        bank.save()
        return response.Response(status=status.HTTP_200_OK, data=bank.amount)
