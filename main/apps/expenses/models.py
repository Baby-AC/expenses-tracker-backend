from django.db import models
from django.contrib.auth.models import User
from django_extensions.db.models import TimeStampedModel

from main.apps.expenses.constants import CURRENCIES_CHOICES, TRANSACTION_TYPE_CHOICES, DEBIT


class Bank(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    currency = models.CharField(max_length=3, choices=CURRENCIES_CHOICES)
    amount = models.DecimalField(max_digits=16, decimal_places=4)
    name = models.CharField(max_length=256)


class Transaction(TimeStampedModel):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=16, decimal_places=4)
    currency = models.CharField(max_length=3, choices=CURRENCIES_CHOICES)
    message = models.CharField(max_length=256, null=True, blank=True)
    transaction_type = models.IntegerField(choices=TRANSACTION_TYPE_CHOICES, default=DEBIT)
