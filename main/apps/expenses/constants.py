EURO = "EUR"
UNITED_STATES_DOLLAR = "USD"
CANADIAN_DOLLAR = "CAD"
AUSTRALIAN_DOLLAR = "AUD"
POUND_STERLING = "GBP"
CHINESE_YUAN = "CNY"

CURRENCIES_CHOICES = (
    (EURO, "Euro"),
    (UNITED_STATES_DOLLAR, "United States Dollar"),
    (CANADIAN_DOLLAR, "Canadian Dollar"),
    (AUSTRALIAN_DOLLAR, "Australian Dollar"),
    (POUND_STERLING, "Pound Sterling"),
    (CHINESE_YUAN, "Chinese Yuan")
)

DEBIT = -1
CREDIT = 1
TRANSACTION_TYPE_CHOICES = (
    (DEBIT, "Debit"),
    (CREDIT, "Credit")
)

EXCHANGE_RATE_URL = "https://www.revolut.com/api/quote/internal/{fromCurrency}{toCurrency}"
